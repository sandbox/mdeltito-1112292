<?php 
/**
 * @file email_list.pages.inc
 * Email List module
 * 2011 - Michael Del Tito <mdeltito@contextllc.com>
 */

define('EMAIL_LIST_MODULE_TABLE', 'email_list_entries');

/**
 * Menu callback for email list form
 */ 
function email_list_user_show($user){
	return drupal_get_form('email_list_user_form', $user);
}

/**
 * Form builder for email list form
 */
function email_list_user_form($form_state, $account) {
	$form = array();
	$form['#theme'] = 'email_list_form_table';
	$form['add_email'] = array(
		'#type' => 'textfield',
		'#title' => t('Add new e-mail address'),
		'#size' => 60,
		'#required' => FALSE
	);
	
	$form['list'] = array();	
	$checkboxes = array();
	$email_list = email_list_load($account);
	
	foreach( $email_list as $entry ) {
		$eid = $entry['entry_id'];

		$checkboxes[$eid] = '';		
		$form[$eid]['email'] = array(
			'#value' => $entry['email']
		);
	}

	$form['checkboxes'] = array(
		'#type' => 'checkboxes',
		'#options' => $checkboxes
	);

	$form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
	
	return $form;	
}

/**
 * Email list form validation callback
 */
function email_list_user_form_validate($form, &$form_state) {
	$email = $form_state['values']['add_email'];
	if( $email != '' && !valid_email_address($email) ) {
		form_set_error('add_email', 'Please enter a valid email address');
	}
}

/**
 * Email list form submission callback
 */
function email_list_user_form_submit($form, &$form_state) {
	global $user;
	$email = $form_state['values']['add_email'];
	$checkboxes = $form_state['values']['checkboxes'];
	
	// add a new email if present
	if( $email != '' ) {
		if( !_email_list_add($user->uid, $email) ) {
			form_set_error('form', 'An error occurred. Please report this to an administrator');
		}
	}	
	
	// delete any selected emails
	foreach( $checkboxes as $entry_id => $delete ) {
		if($delete) {
			db_query("DELETE FROM " . constant('EMAIL_LIST_MODULE_TABLE') . " WHERE entry_id = %d", $entry_id);
		}
	}
}
